<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotaController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
//Route::resource('/user', UserController::class)->middleware('auth');
Route::get('/user/list/{textSearch?}/', [UserController::class, 'listUser']);
Route::post('/user/create/', [UserController::class, 'createUser']);
Route::get('/user/get/{id}/', [UserController::class, 'getUser']);
Route::post('/user/update/', [UserController::class, 'updateUser']);
Route::post('/user/delete/{id}/', [UserController::class, 'deleteUser']);