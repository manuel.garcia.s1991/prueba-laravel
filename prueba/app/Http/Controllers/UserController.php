<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{    
    protected $validator;

     public function listUser($textSearch="")
    {
        try{
            return User::select('id','name','email')->where('name','like', '%'.$textSearch.'%')->orderby('name')->get();        
        }
        catch(Exception $e){
            return response()->json(["message" => $e->getMessage()], 422);
        }
    }

    public function createUser(Request $request)
    {   
        $validator = Validator::make(
            request()->all(),
            [
                'name' => 'required|max:255',
                'email' => 'required|max:255|email|unique:users,email,'.$request->id,
            ]
        );
  
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->getMessageBag()], 422);
        }else{
            try{
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->email);            
                $user->save();
                return (['message' => 'User Saved','status'=>1]);
            }
            catch(Exception $e){
                return response()->json(["message" => $e->getMessage()], $e->status);
            }
        }
        
    }

    public function Updateuser(Request $request)
    {
        $validator = Validator::make(
            request()->all(),
            [
                'name' => 'required|max:255',
                "email" => 'required|max:255|email|unique:users,email,'.$request->id,
            ]
        );
  
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->getMessageBag()], 422);
        }else{
            try{
                $user = User::find($request->id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->email);
                $user->save();
                return (['message' => 'User Updated','status'=>1]);
            }catch(Exception $e){
                return response()->json(["message" => $e->getMessage()], 422);
            }
        }
    }

    public function deleteUser($id)
    {
        try{
            if(auth()->id()<>$id){
                $user = User::find($id);
                $user->delete();
                return (['message' => 'User Deleted','status'=>1]);
            }else{
                return response()->json(["message" => "User cannot delete himself"], 402);
            }
        }catch(Exception $e){
            return response()->json(["message" => $e->getMessage()], 422);
        }
    }

    public function getUser($id)
    {
        try{
            return User::select('id','name','email')->where('id',$id)->first(); 
        }catch(Exception $e){
            return response()->json(["message" => $e->getMessage()], 422);
        }       
    }    
}
