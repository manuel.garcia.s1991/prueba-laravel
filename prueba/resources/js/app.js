require('./bootstrap');
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue';
import VueAlertify from 'vue-alertify';

window.Vue = require('vue').default;

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueAlertify);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.component('list-user', require('./components/ListUser.vue').default);


const app = new Vue({
    el: '#app',
});
